import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/database/countrygetset/countrygetset.dart';
import 'package:untitled/database/database.dart';
import 'package:untitled/details/adddata.dart';

class OtherDetails extends StatefulWidget {
  @override
  State<OtherDetails> createState() => _OtherlDetailsState(

  );
}

class _OtherlDetailsState extends State<OtherDetails> {
  DatabaseHelper mydb = DatabaseHelper();
  late int id;
  late TBLCountry? value;

  TBLCountry? _currentcountry;

  var _textcontroller = TextEditingController();



  void initState() {
    mydb.open();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //Address
        TextField(
          decoration: InputDecoration(
              hintText: 'Your address',
              labelText: 'Address',
              labelStyle: TextStyle(
                fontSize: 17,
                color: Color(0XFF011936),
              ),
              border: OutlineInputBorder()),
          obscureText: false,
          maxLines: 3,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        // city dropdown

        // state
        // country

        // DropdownButton<String>(
        //   hint: Text("select country"),
        //   items: [
        //     DropdownMenuItem(
        //       value: '1',
        //       child: ListView(),
        //     ),
        //     DropdownMenuItem(
        //       value: '2',
        //       child: Text("TWOO"),
        //     ),
        //   ],
        //   onChanged: (_value) => {print(_value.toString())},
        // ),

        // FutureBuilder<List<TBLCountry>>(
        //     future: mydb.getUserList(),
        //     builder: (BuildContext context,
        //         AsyncSnapshot<List<TBLCountry>> snapshot) {
        //       if (!snapshot.hasData) return CircularProgressIndicator();
        //       return DropdownButton<TBLCountry>(
        //         items: snapshot.data
        //             ?.map((user) => DropdownMenuItem<TBLCountry>(
        //                   child: Text(user.CountryName),
        //                   value: user,
        //                 ))
        //             .toList(),
        //         onChanged: (_value) => {
        //           print(_value.toString()),
        //           setState(() {
        //             value=_value;
        //           })
        //         },
        //         isExpanded: false,
        //         //value: _currentUser,
        //         hint: Text('Select User'),
        //       );
        //     }),

        TextField(
          controller: _textcontroller,
          decoration: InputDecoration(
              hintText: 'Countryname',
              labelText: 'Country',
              labelStyle: TextStyle(
                fontSize: 17,
                color: Color(0XFF011936),
              ),
              border: OutlineInputBorder()),
          // obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        //***********************************
        TextField(
          // onTap: (){
          //
          //   // mydb.db.rawInsert("INSERT INTO Mst_Country (CountryName) VALUES (?);",
          //   //     [_textcontroller.text]); //add student from form to database
          //   mydb.db.rawInsert("INSERT INTO Mst_Country (CountryName) VALUES("+_textcontroller.text+","+_textcontroller.text+");");
          //   //_textcontroller.text = "";
          // },
          decoration: InputDecoration(
              hintText: 'Your occupation',
              labelText: 'Occupation',
              labelStyle: TextStyle(
                fontSize: 17,
                color: Color(0XFF011936),
              ),
              border: OutlineInputBorder()),
          obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),
        //education
        TextField(
          decoration: InputDecoration(
              hintText: 'Your education details',
              labelText: 'Education',
              labelStyle: TextStyle(
                fontSize: 17,
                color: Color(0XFF011936),
              ),
              border: OutlineInputBorder()),
          obscureText: false,
        ),
      ],
    );
  }
}
