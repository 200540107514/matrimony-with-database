import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:untitled/database/countrygetset/countrygetset.dart';
import 'package:untitled/database/database.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class AddData extends StatefulWidget {
  @override
  State<AddData> createState() => _AddDataState();
}

class _AddDataState extends State<AddData> {
  DatabaseHelper mydb = DatabaseHelper();
  late int id;

  PickedFile? imageFile;
  final ImagePicker _picker = ImagePicker();

  late TBLCountry? value;

  TBLCountry? _currentcountry;

  late String simage,
      name,
      dob,
      email,
      socialmediaaccount,
      address,
      occupation,
      educationdetails,
      city,
      state,
      country;
  String hobbies="";
  late int age,contactno,gender=1;
  late int height=0,weight=0;

  TextEditingController _namecontroller=new TextEditingController();
  TextEditingController _contactnocontroller=new TextEditingController();
  TextEditingController _addresscontroller=new TextEditingController();
  TextEditingController _emailcontroller=new TextEditingController();
  TextEditingController _heightcontroller=new TextEditingController();
  TextEditingController _weightcontroller=new TextEditingController();
  TextEditingController _socialaccountcontroller=new TextEditingController();
  TextEditingController _countrycontroller=new TextEditingController();
  TextEditingController _occupationcontroller=new TextEditingController();
  TextEditingController _educationcontroller =new TextEditingController();

  bool? reading = false;
  bool? dancing = false;
  bool? playing = false;
  bool? watchingTv = false;
  bool? artsandcraft = false;
  String _selectedGender = 'male';

  var date = new DateTime.now();

  var _datecontroller = new TextEditingController();
  var _agecontroller = new TextEditingController();
  DateTime dateTime = new DateTime.now();

  void initState() {
    mydb.open();
    super.initState();
  }

  Future<void> takePhoto(ImageSource source) async {
    final pickedFile = await _picker.getImage(
      source: source,
    );
    setState(() {
      imageFile = pickedFile;
    });
  }

  Future selectDate(BuildContext context) async {
    final DateTime? newDate = await showDatePicker(
      context: context,
      initialDate: DateTime(date.year - 18, date.month, date.day),
      firstDate: DateTime(1900),
      lastDate: DateTime(date.year - 18, date.month, date.day),
    );
    if (newDate != null) {
      setState(
            () {


              var inputFormat = DateFormat('yyyy-MM-dd HH:mm:ss.SSS');
              var date1 = inputFormat.parse(newDate.toString());

              var outputFormat = DateFormat('dd-MM-yyyy');
              var date2 = outputFormat.format(date1);
              _datecontroller.text = date2.toString();



            dateTime = newDate;
            int a, b;
            a = DateTime.now().year;
            b = newDate.year;
            _agecontroller.text = (a - b).toString();
            age=a-b;



        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // get image START
                Center(
                  child: Stack(
                    children: [
                      CircleAvatar(
                        child: InkWell(
                          onTap: () {
                            showModalBottomSheet(
                              context: context,
                              builder: ((builder) => Container(
                                height:
                                (MediaQuery.of(context).size.height) /
                                    5,
                                width: MediaQuery.of(context).size.width,
                                //margin
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Column(
                                    children: [
                                      Text(
                                        'choose profile Image',
                                        style: TextStyle(
                                          color: Color(0XFFed254e),
                                          fontSize: 25,
                                        ),
                                      ),
                                      Divider(
                                        //  height: 50,
                                        thickness: 1,
                                        color: Color(0XFF011936),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(18.0),
                                        child: Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                          children: [
                                            TextButton.icon(
                                              onPressed: () {
                                                takePhoto(
                                                    ImageSource.camera);
                                              },
                                              icon: Icon(
                                                Icons.camera_alt_rounded,
                                                color: Color(0XFFed254e),
                                                size: 30,
                                              ),
                                              label: Text(
                                                'Camera',
                                                style: TextStyle(
                                                  color: Color(0XFFed254e),
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                            TextButton.icon(
                                              onPressed: () {
                                                takePhoto(
                                                    ImageSource.gallery);
                                              },
                                              icon: Icon(
                                                Icons.image_rounded,
                                                color: Color(0XFFed254e),
                                                size: 30,
                                              ),
                                              label: Text(
                                                'Gallary',
                                                style: TextStyle(
                                                  color: Color(0XFFed254e),
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                            );
                          },
                        ),
                        radius: 80,
                        backgroundColor: Color(0XFF465362),
                        backgroundImage: imageFile == null
                            ? AssetImage("assets/images/defaultimage.png")
                        as ImageProvider
                            : FileImage(File(imageFile!.path)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 18,
                ),
                // GET IMAGE END

                //name
                TextField(
                  controller: _namecontroller,
                  decoration: InputDecoration(
                    hintText: 'Your full name',
                    labelText: 'Name',
                    labelStyle: TextStyle(
                      fontSize: 17,
                      color: Color(0XFF011936),
                    ),
                    border: OutlineInputBorder(),
                  ),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                // datepicker

                TextButton(
                  onPressed: () => selectDate(context),
                  child: Text(
                    "Tap to select your birthdate",
                    style: TextStyle(color: Colors.black, fontSize: 17),
                  ),
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                TextField(
                  controller: _datecontroller,
                  enabled: false,
                  decoration: InputDecoration(
                    hintText: 'Your Date of birth',
                    labelText: 'Date of birth',
                    labelStyle: TextStyle(
                      fontSize: 17,
                      color: Color(0XFF011936),
                    ),
                    border: OutlineInputBorder(),
                  ),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),
// calculate age and display in textfield
                TextField(
                  controller: _agecontroller,
                  enabled: false,
                  decoration: InputDecoration(
                    labelText: 'Age',
                    labelStyle: TextStyle(
                      fontSize: 17,
                      color: Color(0XFF011936),
                    ),
                    border: OutlineInputBorder(),
                  ),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                // gender
                Column(children: [
                  const Text('Select gender:'),
                  ListTile(
                    leading: Radio<String>(
                      value: 'male',
                      groupValue: _selectedGender,
                      onChanged: (value) {
                        setState(() {
                          _selectedGender = value!;
                          gender=0;
                        });
                      },
                    ),
                    title: const Text('Male'),
                  ),
                  ListTile(
                    leading: Radio<String>(
                      value: 'female',
                      groupValue: _selectedGender,
                      onChanged: (value) {
                        setState(() {
                          _selectedGender = value!;
                          gender=1;
                        });
                      },
                    ),
                    title: const Text('Female'),
                  ),
                ]),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                TextField(
                  controller: _contactnocontroller,
                  decoration: InputDecoration(
                    hintText: 'Your contact no',
                    labelText: 'Contact no',
                    labelStyle: TextStyle(
                      fontSize: 17,
                      color: Color(0XFF011936),
                    ),
                    border: OutlineInputBorder(),
                  ),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                TextField(
                  controller: _emailcontroller,
                  decoration: InputDecoration(
                    hintText: 'Your mail id',
                    labelText: 'Email',
                    labelStyle: TextStyle(
                      fontSize: 17,
                      color: Color(0XFF011936),
                    ),
                    border: OutlineInputBorder(),
                  ),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                TextField(
                  controller: _heightcontroller,
                  decoration: InputDecoration(
                      hintText: 'Your height',
                      labelText: 'Height',
                      labelStyle: TextStyle(
                        fontSize: 17,
                        color: Color(0XFF011936),
                      ),
                      border: OutlineInputBorder()),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                TextField(
                  controller: _weightcontroller,
                  decoration: InputDecoration(
                    hintText: 'Your weight',
                    labelText: 'Weight',
                    labelStyle: TextStyle(
                      fontSize: 17,
                      color: Color(0XFF011936),
                    ),
                    border: OutlineInputBorder(),
                  ),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

// hobbies

                Text('Select Hobbies:'),

                Column(children: [
                  Row(
                    children: [
                      Checkbox(
                          value: reading,
                          onChanged: (bool? value) {
                            setState(() {
                              reading = value;
                              hobbies=hobbies+'reading';
                            });
                          }),
                      Text("Reading"),
                    ],
                  )
                ]),
                Column(children: [
                  Row(
                    children: [
                      Checkbox(
                          value: dancing,
                          onChanged: (bool? value) {
                            setState(() {
                              dancing = value;
                              hobbies=hobbies+'dancing';
                            });
                          }),
                      Text("Dancing"),
                    ],
                  )
                ]),
                Column(children: [
                  Row(
                    children: [
                      Checkbox(
                          value: playing,
                          onChanged: (bool? value) {
                            setState(() {
                              playing = value;
                              hobbies=hobbies+'playing';
                            });
                          }),
                      Text("Playing"),
                    ],
                  )
                ]),
                Column(children: [
                  Row(
                    children: [
                      Checkbox(
                          value: watchingTv,
                          onChanged: (bool? value) {
                            setState(() {
                              watchingTv = value;
                              hobbies=hobbies+'watchingTv';
                            });
                          }),
                      Text("Watching Tv"),
                    ],
                  )
                ]),
                Column(children: [
                  Row(
                    children: [
                      Checkbox(
                          value: artsandcraft,
                          onChanged: (bool? value) {
                            setState(() {
                              artsandcraft = value;
                              hobbies=hobbies+'artsandcraft';
                            });
                          }),
                      Text("Arts and Craft"),
                    ],
                  )
                ]),

                TextField(
                  controller: _socialaccountcontroller,
                  decoration: InputDecoration(
                    hintText: 'Social media account',
                    labelText: 'Social accounts',
                    labelStyle: TextStyle(
                      fontSize: 17,
                      color: Color(0XFF011936),
                    ),
                    border: OutlineInputBorder(),
                  ),
                  obscureText: false,
                  maxLines: 3,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                //Address
                TextField(
                  controller: _addresscontroller,
                  decoration: InputDecoration(
                      hintText: 'Your address',
                      labelText: 'Address',
                      labelStyle: TextStyle(
                        fontSize: 17,
                        color: Color(0XFF011936),
                      ),
                      border: OutlineInputBorder()),
                  obscureText: false,
                  maxLines: 3,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                // city dropdown

                // state
                // country

                // DropdownButton<String>(
                //   hint: Text("select country"),
                //   items: [
                //     DropdownMenuItem(
                //       value: '1',
                //       child: ListView(),
                //     ),
                //     DropdownMenuItem(
                //       value: '2',
                //       child: Text("TWOO"),
                //     ),
                //   ],
                //   onChanged: (_value) => {print(_value.toString())},
                // ),

                // FutureBuilder<List<TBLCountry>>(
                //     future: mydb.getUserList(),
                //     builder: (BuildContext context,
                //         AsyncSnapshot<List<TBLCountry>> snapshot) {
                //       if (!snapshot.hasData) return CircularProgressIndicator();
                //       return DropdownButton<TBLCountry>(
                //         items: snapshot.data
                //             ?.map((user) => DropdownMenuItem<TBLCountry>(
                //                   child: Text(user.CountryName),
                //                   value: user,
                //                 ))
                //             .toList(),
                //         onChanged: (_value) => {
                //           print(_value.toString()),
                //           setState(() {
                //             value=_value;
                //           })
                //         },
                //         isExpanded: false,
                //         //value: _currentUser,
                //         hint: Text('Select User'),
                //       );
                //     }),

                TextField(
                  controller: _countrycontroller,
                  decoration: InputDecoration(
                      hintText: 'Countryname',
                      labelText: 'Country',
                      labelStyle: TextStyle(
                        fontSize: 17,
                        color: Color(0XFF011936),
                      ),
                      border: OutlineInputBorder()),
                  // obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                TextField(
                  controller: _occupationcontroller,
                  // onTap: (){
                  //
                  //   // mydb.db.rawInsert("INSERT INTO Mst_Country (CountryName) VALUES (?);",
                  //   //     [_textcontroller.text]); //add student from form to database
                  //   mydb.db.rawInsert("INSERT INTO Mst_Country (CountryName) VALUES("+_textcontroller.text+","+_textcontroller.text+");");
                  //   //_textcontroller.text = "";
                  // },
                  decoration: InputDecoration(
                      hintText: 'Your occupation',
                      labelText: 'Occupation',
                      labelStyle: TextStyle(
                        fontSize: 17,
                        color: Color(0XFF011936),
                      ),
                      border: OutlineInputBorder()),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),
                //education
                TextField(
                  controller: _educationcontroller,
                  decoration: InputDecoration(
                      hintText: 'Your education details',
                      labelText: 'Education',
                      labelStyle: TextStyle(
                        fontSize: 17,
                        color: Color(0XFF011936),
                      ),
                      border: OutlineInputBorder()),
                  obscureText: false,
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 30,
                ),

                TextButton(
                  style:
                  TextButton.styleFrom(backgroundColor: Color(0XFFed254e)),
                  // setState(() {
                  //
                  // });
                  onPressed: () {
                    setState(() {

                      // simage=imageFile.();
                      // name=_namecontroller;
                      // dob=_datecontroller.toString();
                      // height=_heightcontroller;
                      // weight=_weightcontroller;
                      // contactno=_contactnocontroller;
                      // email=_emailcontroller;
                      // socialmediaaccount=_socialaccountcontroller;
                      // address=_addresscontroller;
                      // occupation=_occupationcontroller;
                      // educationdetails=_educationcontroller.toString();

                      mydb.db.rawInsert(
                          "INSERT INTO user_table (UserName,ProfileImage,DOB,Age,Height,Weight,Gender,ContactNo,Email,SocialMediaAccount,Hobbies,Address,Occupation,EducationDetails,City,State,Country) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
                          [
                            _namecontroller.text,
                            imageFile.toString(),
                            _datecontroller.text,
                            _agecontroller.text,
                            _heightcontroller.text,
                            _weightcontroller.text,
                            gender,
                            _contactnocontroller.text,
                            _emailcontroller.text,
                            _socialaccountcontroller.text,
                            hobbies,
                            _addresscontroller.text,
                            _occupationcontroller.text,
                            _educationcontroller.text,
                            "rajkot",
                            "Gujarat",
                            "Country",
                          ]);
                    });
                    print('-------------Insert data successfull--------------');
                    //add user  form to database
                    //mydb.db.rawInsert("INSERT INTO Mst_Country (CountryName) VALUES("+_textcontroller.text+","+_textcontroller.text+");");
                    //_textcontroller.text = "";
                  },
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: 17,
                      color: Color(0XFFf4fffd),
                    ),
                  ),
                ),
                SizedBox(
                  height: (MediaQuery.of(context).size.height) / 28,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
