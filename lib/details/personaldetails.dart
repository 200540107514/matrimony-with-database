import 'dart:ffi';

import 'package:image_picker/image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

class PersonalDetails extends StatefulWidget {
  @override
  State<PersonalDetails> createState() => _PersonalDetailsState();
}

class _PersonalDetailsState extends State<PersonalDetails> {
   PickedFile? imageFile;
  final ImagePicker _picker = ImagePicker();

  String _selectedGender = 'male';

  var date = DateTime.now();
  var _datecontroller = TextEditingController();
  var _agecontroller = TextEditingController();
  DateTime dateTime = DateTime.now();

  Future<void> takePhoto(ImageSource source) async {
    final pickedFile = await _picker.getImage(
      source: source,
    );
    setState(() {
      imageFile = pickedFile;
    });
  }

  bool? reading = false;
  bool? dancing = false;
  bool? playing = false;
  bool? watchingTv = false;
  bool? artsandcraft = false;

  Future selectDate(BuildContext context) async {
    final DateTime? newDate = await showDatePicker(
      context: context,
      initialDate: DateTime(date.year - 18, date.month, date.day),
      firstDate: DateTime(1900),
      lastDate: DateTime(date.year - 18, date.month, date.day),
    );
    if (newDate != null) {
      setState(
        () {
          _datecontroller.text = newDate.toString();
          dateTime = newDate;
          int a, b;
          a = DateTime.now().year;
          b = newDate.year;
          _agecontroller.text = (a - b).toString();
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // get image START
        Center(
          child: Stack(
            children: [
              CircleAvatar(
                child: InkWell(
                  onTap: () {
                    showModalBottomSheet(
                      context: context,
                      builder: ((builder) => Container(
                            height: (MediaQuery.of(context).size.height) / 5,
                            width: MediaQuery.of(context).size.width,
                            //margin
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                children: [
                                  Text(
                                    'choose profile Image',
                                    style: TextStyle(
                                      color: Color(0XFFed254e),
                                      fontSize: 25,
                                    ),
                                  ),
                                  Divider(
                                    //  height: 50,
                                    thickness: 1,
                                    color: Color(0XFF011936),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(18.0),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        TextButton.icon(
                                          onPressed: () {

                                            takePhoto(ImageSource.camera);
                                          },
                                          icon: Icon(
                                            Icons.camera_alt_rounded,
                                            color: Color(0XFFed254e),
                                            size: 30,
                                          ),
                                          label: Text(
                                            'Camera',
                                            style: TextStyle(
                                              color: Color(0XFFed254e),
                                              fontSize: 20,
                                            ),
                                          ),
                                        ),
                                        TextButton.icon(
                                          onPressed: () {

                                            takePhoto(ImageSource.gallery);
                                          },
                                          icon: Icon(
                                            Icons.image_rounded,
                                            color: Color(0XFFed254e),
                                            size: 30,
                                          ),
                                          label: Text(
                                            'Gallary',
                                            style: TextStyle(
                                              color: Color(0XFFed254e),
                                              fontSize: 20,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )),
                    );

                    // Container(
                    //   height: 100,
                    //   width: MediaQuery.of(context).size.width,
                    //   //margin
                    //   child: Column(
                    //     children: [
                    //       Text('chose profile'),
                    //       Row(
                    //         children: [
                    //           TextButton.icon(
                    //             onPressed: () {
                    //               takePhoto(ImageSource.camera);
                    //             },
                    //             icon: Icon(Icons.camera_alt_rounded),
                    //             label: Text('Camera'),
                    //           ),
                    //           TextButton.icon(
                    //             onPressed: () {
                    //               takePhoto(ImageSource.gallery);
                    //             },
                    //             icon: Icon(Icons.browse_gallery_rounded),
                    //             label: Text('Gaallary'),
                    //           ),
                    //         ],
                    //       )
                    //     ],
                    //   ),
                    // );
                  },

                ),
                radius: 80,
                backgroundColor: Color(0XFF465362),
                backgroundImage: imageFile == null
                    ? AssetImage("assets/images/defaultimage.png")as ImageProvider
                    :FileImage(File(imageFile!.path)),
              ),
            ],
          ),
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 18,
        ),
        // GET IMAGE END

        //name
        TextField(
          decoration: InputDecoration(
            hintText: 'Your full name',
            labelText: 'Name',
            labelStyle: TextStyle(
              fontSize: 17,
              color: Color(0XFF011936),
            ),
            border: OutlineInputBorder(),
          ),
          obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        // datepicker

        TextButton(
          onPressed: () => selectDate(context),
          child: Text(
            "Tap to select your birthdate",
            style: TextStyle(color: Colors.black, fontSize: 17),
          ),
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        TextField(
          controller: _datecontroller,
          enabled: false,
          decoration: InputDecoration(
            hintText: 'Your Date of birth',
            labelText: 'Date of birth',
            labelStyle: TextStyle(
              fontSize: 17,
              color: Color(0XFF011936),
            ),
            border: OutlineInputBorder(),
          ),
          obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),
// calculate age and display in textfield
        TextField(
          controller: _agecontroller,
          enabled: false,
          decoration: InputDecoration(
            labelText: 'Age',
            labelStyle: TextStyle(
              fontSize: 17,
              color: Color(0XFF011936),
            ),
            border: OutlineInputBorder(),
          ),
          obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        // gender
        Column(children: [
          const Text('Select gender:'),
          ListTile(
            leading: Radio<String>(
              value: 'male',
              groupValue: _selectedGender,
              onChanged: (value) {
                setState(() {
                  _selectedGender = value!;
                });
              },
            ),
            title: const Text('Male'),
          ),
          ListTile(
            leading: Radio<String>(
              value: 'female',
              groupValue: _selectedGender,
              onChanged: (value) {
                setState(() {
                  _selectedGender = value!;
                });
              },
            ),
            title: const Text('Female'),
          ),
        ]),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        TextField(
          decoration: InputDecoration(
            hintText: 'Your contact no',
            labelText: 'Contact no',
            labelStyle: TextStyle(
              fontSize: 17,
              color: Color(0XFF011936),
            ),
            border: OutlineInputBorder(),
          ),
          obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        TextField(
          decoration: InputDecoration(
            hintText: 'Your mail id',
            labelText: 'Email',
            labelStyle: TextStyle(
              fontSize: 17,
              color: Color(0XFF011936),
            ),
            border: OutlineInputBorder(),
          ),
          obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        TextField(
          decoration: InputDecoration(
              hintText: 'Your height',
              labelText: 'Height',
              labelStyle: TextStyle(
                fontSize: 17,
                color: Color(0XFF011936),
              ),
              border: OutlineInputBorder()),
          obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

        TextField(
          decoration: InputDecoration(
            hintText: 'Your weight',
            labelText: 'Weight',
            labelStyle: TextStyle(
              fontSize: 17,
              color: Color(0XFF011936),
            ),
            border: OutlineInputBorder(),
          ),
          obscureText: false,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),

// hobbies

        Text('Select Hobbies:'),

        Column(children: [
          Row(
            children: [
              Checkbox(
                  value: reading,
                  onChanged: (bool? value) {
                    setState(() {
                      reading = value;
                    });
                  }),
              Text("Reading"),
            ],
          )
        ]),
        Column(children: [
          Row(
            children: [
              Checkbox(
                  value: dancing,
                  onChanged: (bool? value) {
                    setState(() {
                      dancing = value;
                    });
                  }),
              Text("Dancing"),
            ],
          )
        ]),
        Column(children: [
          Row(
            children: [
              Checkbox(
                  value: playing,
                  onChanged: (bool? value) {
                    setState(() {
                      playing = value;
                    });
                  }),
              Text("Playing"),
            ],
          )
        ]),
        Column(children: [
          Row(
            children: [
              Checkbox(
                  value: watchingTv,
                  onChanged: (bool? value) {
                    setState(() {
                      watchingTv = value;
                    });
                  }),
              Text("Watching Tv"),
            ],
          )
        ]),
        Column(children: [
          Row(
            children: [
              Checkbox(
                  value: artsandcraft,
                  onChanged: (bool? value) {
                    setState(() {
                      artsandcraft = value;
                    });
                  }),
              Text("Arts and Craft"),
            ],
          )
        ]),

        TextField(
          decoration: InputDecoration(
            hintText: 'Social media account',
            labelText: 'Social accounts',
            labelStyle: TextStyle(
              fontSize: 17,
              color: Color(0XFF011936),
            ),
            border: OutlineInputBorder(),
          ),
          obscureText: false,
          maxLines: 3,
        ),
        SizedBox(
          height: (MediaQuery.of(context).size.height) / 30,
        ),
      ],
    );
  }
}
