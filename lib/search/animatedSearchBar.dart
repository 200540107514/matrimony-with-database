import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class AnimatedSearchBar extends StatefulWidget {
  @override
  State<AnimatedSearchBar> createState() => _AnimatedSearchBarState();
}

class _AnimatedSearchBarState extends State<AnimatedSearchBar> {
  bool folded = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AnimatedContainer(
      duration: Duration(milliseconds: 400),
      alignment: AlignmentDirectional.topEnd,
      width: folded ? 56 : (MediaQuery.of(context).size.width) - 30,
      height: 56,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(32),
        color: Color(0XFF011936),
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 16),
              child: !folded
                  ? TextField(
                      cursorColor: Color(0XFFed254e),
                      style: TextStyle(
                        color: Color(0XFFf9dc5c),
                      ),
                      decoration: InputDecoration
                      ( hintText: 'Search',
                        hintStyle: TextStyle(color: Color(0XFFf4fffd)),
                        border: InputBorder.none,
                        fillColor: Color(0XFFed254e),

                      ),

                    )
                  : null,
            ),
          ),
          AnimatedContainer(
            duration: Duration(milliseconds: 400),
            child: Material(
              type: MaterialType.transparency,
              child: InkWell(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(folded ? 32 : 0),
                  topRight: Radius.circular(32),
                  bottomLeft: Radius.circular(folded ? 32 : 0),
                  bottomRight: Radius.circular(32),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Icon(
                    folded ? Icons.search_rounded : Icons.close_rounded,
                    color: Color(0XFFf4fffd),
                  ),
                ),
                onTap: () {
                  setState(() {
                    folded = !folded;
                  });
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
