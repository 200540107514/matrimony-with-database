import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/colors/color.dart';
import 'package:untitled/search/animatedSearchBar.dart';
import 'package:untitled/database/database.dart';
import 'package:untitled/database/usergetset.dart';
import 'package:untitled/details/adddata.dart';

class Dashbord extends StatefulWidget {
  @override
  State<Dashbord> createState() => _DashbordState();
}

class _DashbordState extends State<Dashbord> {
  late int id;
  bool isFavourite = false;
  int fav = 0;

  DatabaseHelper mydb = new DatabaseHelper();
  void initState() {
    mydb.open();
    mydb.getUserList();
    mydb.getFullUserList();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AnimatedSearchBar(),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: FutureBuilder<List<TBLUser>>(
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      //child: Text(snapshot.data![index].CountryID.toString()),
                      child: Column(
                        children: [
                          Text(snapshot.data![index].UserName.toString()),
                          Text(
                            snapshot.data![index].UserName.toString(),
                            style: TextStyle(
                              fontSize: 17,
                              color: Colors.black,
                            ),
                          ),
                          Text(
                            snapshot.data![index].Favorite.toString(),
                            style: TextStyle(
                              fontSize: 17,
                              color: Colors.black,
                            ),
                          ),
                          TextButton(
                            child: Text('delet'),
                            onPressed: () async {
                              mydb.deleteUser(snapshot.data![index].UserID);
                              mydb.getFullUserList();
                            },
                          ),
                          IconButton(
                              icon: Icon(
                                Icons.favorite_rounded,
                              ),
                              iconSize: 50,
                              color: isFavourite ? Colors.red : Colors.grey,
                              splashColor: Colors.transparent,
                              onPressed: () {
                                setState(() {
                                  isFavourite = !isFavourite;
                                  if (isFavourite == false) {
                                    fav = 0;
                                  } else {
                                    fav = 1;
                                  }
                                  mydb.db.rawInsert(
                                      "UPDATE user_table SET Favourite=? WHERE UserID=?",
                                      [fav,snapshot.data![index].UserID]);
                                  print('------------- successfull--------------');
                                });
                              })
                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                    id = snapshot.data![index].UserID;
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AddData()),
                    );
                  },
                );
              },
              itemCount: snapshot.data!.length,
            );
          }
          else
            {
              return Image.asset('assets/images/defaultimage.png');
            }
        },
        future: DatabaseHelper().getFullUserList(),
      ),
    );
  }
}
