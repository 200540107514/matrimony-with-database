import 'package:flutter/material.dart';

class NavigationBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Container(
        color: Colors.black,
        padding: EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          // Optional
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          // Change to your own spacing
          children: [
            GestureDetector(
              child: Hero(
                tag: "null",
                child: Icon(
                  Icons.home_filled,
                  color: Colors.grey,
                  size: 32,
                ),
              ),
              // onTap: () {
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (context) => HomePage()));
              // },
            ),
            Icon(
              Icons.search_outlined,
              color: Colors.grey,
              size: 32,
            ),
            Icon(
              Icons.add_rounded,
              color: Colors.grey,
              size: 32,
            ),
            Icon(
             Icons.home,
              color: Colors.grey,
              size: 28,
            ),
            GestureDetector(
              child: Hero(
                tag: "null",
                child: Icon(
                  Icons.person,
                  color: Colors.grey,
                  size: 32,
                ),
              ),
              // onTap: () {
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (context) => AccountPage()));
              // },
            ),
          ],
        ),
      ),
    );
  }
}
