import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:untitled/colors/color.dart';
import 'package:untitled/dashbord.dart';
import 'package:untitled/database/database.dart';
import 'package:untitled/details/adddata.dart';
import 'package:untitled/details/details.dart';
import 'package:untitled/favoutir/favoutir.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {



  void initState(){
    DatabaseHelper().copyPasteAssetFileToRoot().then((value) => {

      print('DATABASE INITIALIZED SUCCESSFULLY')
     // DatabaseHelper().search().then((value)=>{})
    });

    super.initState();
  }


  int index=1;

  final screens=[
    AddData(),
    Dashbord(),
    UserListScreen(),
  ];

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    final items=[
      Icon(Icons.person_add_rounded, size: 25,color:Colors.blueGrey[300],),
      Icon(Icons.home, size: 25,color: Colors.blueGrey[300]),
      Icon(Icons.favorite_rounded, size: 25,color: Colors.blueGrey[300],),
    ];

    return MaterialApp(
      title: 'Matrimony',
      theme: ThemeData(

        primarySwatch: Colors.yellow,
        primaryColor: themered,
        backgroundColor: themewhite,
        errorColor: themered,
      ),
      home: Scaffold(
        extendBody: true,
        body: screens[index],
        bottomNavigationBar: CurvedNavigationBar(

 //colors
          color:Color(0XFF011936),
          buttonBackgroundColor: Color(0XFFed254e),
          backgroundColor: Colors.transparent,
          height: 50,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 300),
          index: index,
          items: items,

          onTap: (index)=> setState(() => this.index=index),
        ),
      ),
    );
  }
}