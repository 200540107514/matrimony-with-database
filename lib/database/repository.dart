
import 'package:sqflite/sqflite.dart';
import 'package:untitled/database/database.dart';

class Repository {

 late DatabaseHelper _databaseHelper;

 Repository()
 {
   _databaseHelper=DatabaseHelper();

 }
 late Database _database;

 Future<Database> get database async{
   if(_database != null)return _database;
   _database=await _databaseHelper.initDatabase();
   return _database;
 }

 Future insertData(table,data) async{
   var connection= await database;
   return await connection.insert(table,data);
 }


}