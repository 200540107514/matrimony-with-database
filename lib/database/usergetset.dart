import 'dart:ffi';

class TBLUser {
  late int _UserID;
  late String _UserName;
  late String _ProfileImage;
  late String _DOB;
  late int _Age;
  late int _Height;
  late int _Weight;
  late int _Gender;
  late int _ContactNo;
  late String _Email;
  late String _SocialMediaAccount;
  late String _Hobbies;
  late String _Address;
  late String _Occupation;
  late String _EducationDetails;
  late String _City;
  late String _State;
  late String _Country;

  late int _Favorite;

  int get UserID => _UserID;
  int get Gender => _Gender;
  int get Age => _Age;
  int get ContactNo => _ContactNo;

  int get Favorite => _Favorite;
  String get ProfileImage => _ProfileImage;
  String get DOB => _DOB;
  String get UserName => _UserName;
  String get Email => _Email;
  String get SocialMediaAccount => _SocialMediaAccount;
  String get Hobbies => _Hobbies;
  String get Address => _Address;
  String get Occupation => _Occupation;
  String get EducationDetails => _EducationDetails;
  String get City => _City;
  String get State => _State;
  String get Country => _Country;

  int get Height => _Height;
  int get Weight => _Weight;

  TBLUser.fromMap(Map<String, Object?> map) {
    _UserID = map['UserID'] != null ? map['UserID'] as int : 0;
    print("USER ID");
    _Age = map['Age'] != null ? map['Age'] as int : 0;
    print("AGE");
    _Gender = map['Gender'] != null ? map['Gender'] as int : 0;
    print("GENDER");
    _ContactNo = map['ContactNo'] != null ? map['ContactNo'] as int : 0;
    print("Contact");

    _ProfileImage =map['ProfileImage'] != null ? map['ProfileImage'] as String : '';
    print("ProfileImage");
    _DOB =map['DOB'] != null ? map['DOB'] as String : '';
    print("DOB");
    _UserName =map['UserName'] != null ? map['UserName'] as String : '';
    print("UserName");
    _Email =map['Email'] != null ? map['Email'] as String : '';
    print("Email");
    _SocialMediaAccount =map['SocialMediaAccount'] != null ? map['SocialMediaAccount'] as String : '';
    print("SocialMediaAccount");
    _Hobbies =map['Hobbies'] != null ? map['Hobbies'] as String : '';
    print("Hobbies");
    _Address =map['Address'] != null ? map['Address'] as String : '';
    print("Address");
    _Occupation =map['Occupation'] != null ? map['Occupation'] as String : '';
    print("Occupation");
    _EducationDetails =map['EducationDetails'] != null ? map['EducationDetails'] as String : '';
    print("EducationDetails");
    _City =map['City'] != null ? map['City'] as String : '';
    print("City");
    _State =map['State'] != null ? map['State'] as String : '';
    print("State");
    _Country =map['Country'] != null ? map['Country'] as String : '';
    print("Country");

    _Height =map['Height'] != null ? map['Height'] as int : 0;
    print("Height");
    _Weight =map['Weight'] != null ? map['Weight'] as int : 0;
    print("Weight");
    _Favorite =map['Favorite'] != null ? map['Favorite'] as int : 0;
    print("Favorite");
  }
}

