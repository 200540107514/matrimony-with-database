import 'dart:io';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:untitled/database/countrygetset/countrygetset.dart';
import 'package:untitled/database/usergetset.dart';

class DatabaseHelper {

 late Database db;
  Future open() async {
    db = await initDatabase();
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'Matrimony_2022.db');
    //join is from path package
    print(path); //output /data/user/0/com.testapp.flutter.testapp/databases/demo.db
  }

 Future<List<TBLCountry>> getUserList() async {
   List<TBLCountry> list = [];
   Database db = await initDatabase();
   List<Map<String, Object?>> userList =
   await db.rawQuery('SELECT * FROM Mst_Country');
   for(int i = 0;i<userList.length;i++){
     TBLCountry model = TBLCountry.fromMap(userList[i]);

     list.add(model);
   }
   return list;
 }
 Future<List<TBLUser>> getFullUserList() async {
   List<TBLUser> list = [];
   Database db = await initDatabase();
   List<Map<String, Object?>> userList =
   await db.rawQuery('SELECT * FROM user_table');
   for(int i = 0;i<userList.length;i++){
     TBLUser model = TBLUser.fromMap(userList[i]);
     list.add(model);
   }
   return list;
 }



 Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'Matrimony_2022.db');
    return await openDatabase(
      databasePath,
      version: 1,
      onCreate: (db, version) {},
      onUpgrade: (db, oldVersion, newVersion) {
        db.execute('ALTER ');
        db.execute('sql');
      },
    );
  }


 Future<int> delete(int id) async {
   var dbClient = await db;
   return await dbClient.delete( "Mst_Country", where: "CountryID = ?", whereArgs: [id]);
 }

 Future<int> deleteUser(int id) async {
   var dbClient = await db;
   return await dbClient.delete( "user_table", where: "UserID = ?", whereArgs: [id]);
 }

 // Future<int> update(Employee employee) async {
 //   var dbClient = await db;
 //   return await dbClient.update(TABLE, employee.toMap(),
 //       where: '$ID = ?', whereArgs: [employee.id]);
 // }


  Future<void> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "Matrimony_2022.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data = await rootBundle
          .load(join('assets/databases', 'Matrimony_2022.db'));
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await new File(path).writeAsBytes(bytes);
    }
  }


}